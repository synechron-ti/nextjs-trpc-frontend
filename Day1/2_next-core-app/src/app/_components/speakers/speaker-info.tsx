'use client'

// CSR: Client Side Rendering
import styles from '@/app/conference/conference.module.css';

import React, { useEffect } from 'react';
import { Speaker } from '@/app/conference/models/speaker';

const SpeakerInfo = ({ id }: { id: string }) => {
    const [speaker, setSpeaker] = React.useState({} as Speaker);

    useEffect(() => {
        (async () => {
            try {
                const response = await fetch(`http://localhost:8001/speakers/${id}`);
                const data = await response.json();
                setSpeaker(data);
            } catch (err) {
                console.error(err);
            }
        })()
    }, [id]);

    return (
        <div className={styles.infoContainer}>
            <div className={styles.descText}>
                Last Rendered: {new Date().toLocaleTimeString()}
            </div>
            {
                speaker ? <div>
                    <h3 className={styles.titleText}>{speaker.name}</h3>
                    <h5 className={styles.descText}>About: {speaker.bio}</h5>
                    {speaker.sessions &&
                        speaker.sessions.map(({ name, id }) => (
                            <div key={id}>
                                <h5 className={styles.descText}>Session: {name}</h5>
                            </div>
                        ))}
                </div> : <h3 className={styles.titleText}>No Details for the Speaker Found</h3>
            }
        </div>
    );
};

export default SpeakerInfo;