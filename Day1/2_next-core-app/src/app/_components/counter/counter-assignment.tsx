'use client'

// ------------------------------------------------- Using useCallback & React.memo

import React, { useCallback, useRef, useState } from 'react';

interface CounterProps {
    interval?: number;
}

const Counter: React.FC<CounterProps> = ({ interval = 1 }) => {
    const [count, setCount] = useState(0);
    const [flag, setFlag] = useState(false);
    const clickCount = useRef(0);

    // console.log("Counter is Rendered: ", clickCount.current);

    const manageClickCount = useCallback(() => {
        clickCount.current++;
        if (clickCount.current > 9) {
            setFlag(true);
        }
    }, []);

    const inc = useCallback(() => {
        setCount(prevCount => prevCount + interval);
        manageClickCount();
    }, [interval, manageClickCount]);

    const dec = useCallback(() => {
        setCount(prevCount => prevCount - interval);
        manageClickCount();
    }, [interval, manageClickCount])

    const reset = useCallback(() => {
        setCount(0);
        setFlag(false);
        clickCount.current = 0;
    }, []);

    return (
        <>
            <div className="text-center">
                <h3 className="text-info">Counter Component</h3>
            </div>
            <div className="d-grid gap-2 mx-auto col-6">
                <input type="text" className="form-control form-control-lg" value={count} readOnly />
            </div>
            <ChildComponent inc={inc} dec={dec} reset={reset} flag={flag} />
        </>
    );
}

interface ChildComponentProps {
    inc: () => void;
    dec: () => void;
    reset: () => void;
    flag: boolean;
}

const ChildComponent: React.FC<ChildComponentProps> = React.memo(function ChildComponent({ inc, dec, reset, flag }) {
    console.log("Child Component is Rendered");

    return (
        <div className="d-grid gap-2 mx-auto col-6 mt-3">
            <button className="btn btn-info" disabled={flag} onClick={inc}>
                <span className='fs-4'>+</span>
            </button>
            <button className="btn btn-info" disabled={flag} onClick={dec}>
                <span className='fs-4'>-</span>
            </button>
            <button className="btn btn-secondary" disabled={!flag} onClick={reset}>
                <span className='fs-4'>Reset</span>
            </button>
        </div>
    );
});

const CounterAssignment: React.FC = () => (
    <div>
        <Counter />
    </div>
);

export default CounterAssignment;

// ------------------------------------------------- Using Function

// import React, { useRef, useState } from 'react';

// interface CounterProps {
//     interval?: number;
// }

// const Counter: React.FC<CounterProps> = ({ interval = 1 }) => {
//     const [count, setCount] = useState(0);
//     const [flag, setFlag] = useState(false);
//     let clickCount = useRef(0);

//     // console.log("Counter is Rendered: ", clickCount.current);

//     const manageClickCount = () => {
//         clickCount.current++;
//         if (clickCount.current > 9) {
//             setFlag(true);
//         }
//     }

//     const inc = () => {
//         setCount(prevCount => prevCount + interval);
//         manageClickCount();
//     }

//     const dec = () => {
//         setCount(prevCount => prevCount - interval);
//         manageClickCount();
//     }

//     const reset = () => {
//         setCount(0);
//         setFlag(false);
//         clickCount.current = 0;
//     }

//     return (
//         <>
//             <div className="text-center">
//                 <h3 className="text-info">Counter Component</h3>
//             </div>
//             <div className="d-grid gap-2 mx-auto col-6">
//                 <input type="text" className="form-control form-control-lg" value={count} readOnly />
//                 <button className="btn btn-info" disabled={flag} onClick={inc}>
//                     <span className='fs-4'>+</span>
//                 </button>
//                 <button className="btn btn-info" disabled={flag} onClick={dec}>
//                     <span className='fs-4'>-</span>
//                 </button>
//                 <button className="btn btn-secondary" disabled={!flag} onClick={reset}>
//                     <span className='fs-4'>Reset</span>
//                 </button>
//             </div>
//         </>
//     );
// }

// const CounterAssignment: React.FC = () => (
//     <div>
//         <Counter />
//         <hr />
//         <Counter interval={10} />
//     </div>
// );

// export default CounterAssignment;

// ------------------------------------------------- Using Class
// import React, { Component } from 'react';

// interface CounterProps {
//     interval: number;
// }

// interface CounterState {
//     count: number;
//     flag: boolean;
// }

// class Counter extends Component<CounterProps, CounterState> {
//     static defaultProps = {
//         interval: 1
//     };

//     private clickCount: number;

//     constructor(props: CounterProps) {
//         super(props);
//         this.state = { count: 0, flag: false };
//         this.clickCount = 0;
//         this.inc = this.inc.bind(this);
//         this.dec = this.dec.bind(this);
//         this.reset = this.reset.bind(this);
//     }

//     manageClickCount() {
//         this.clickCount++;
//         if (this.clickCount > 9) {
//             this.setState({ flag: true });
//         }
//     }

//     inc() {
//         // this.setState({ count: this.state.count + this.props.interval });
//         this.setState(
//             (prevState) => ({ count: prevState.count + this.props.interval }),
//             () => {
//                 this.manageClickCount();
//             }
//         );
//     }

//     dec() {
//         // this.setState({ count: this.state.count - this.props.interval });
//         this.setState(
//             (prevState) => ({ count: prevState.count - this.props.interval }),
//             () => {
//                 this.manageClickCount();
//             }
//         );
//     }

//     reset() {
//         this.clickCount = 0;
//         this.setState({ count: 0, flag: false });
//     }


//     render() {
//         return (
//             <>
//                 <div className="text-center">
//                     <h3 className="text-info">Counter Component</h3>
//                 </div>
//                 <div className="d-grid gap-2 mx-auto col-6">
//                     <input type="text" className="form-control form-control-lg" value={this.state.count} readOnly />
//                     <button className="btn btn-info" disabled={this.state.flag} onClick={this.inc}>
//                         <span className='fs-4'>+</span>
//                     </button>
//                     <button className="btn btn-info" disabled={this.state.flag} onClick={this.dec}>
//                         <span className='fs-4'>-</span>
//                     </button>
//                     <button className="btn btn-secondary" disabled={!this.state.flag} onClick={this.reset}>
//                         <span className='fs-4'>Reset</span>
//                     </button>
//                 </div>
//             </>
//         );
//     }
// }

// class CounterAssignment extends Component {
//     render() {
//         return (
//             <div>
//                 <Counter />
//                 <hr />
//                 <Counter interval={10} />
//             </div>
//         );
//     }
// }

// export default CounterAssignment;