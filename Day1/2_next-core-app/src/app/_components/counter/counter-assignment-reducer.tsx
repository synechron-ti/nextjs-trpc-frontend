'use client'

// ------------------------------------------------- Using useReducer

import React, { useCallback, useReducer, useRef } from 'react';

interface CounterProps {
    interval?: number;
}

interface CounterState {
    count: number;
    flag: boolean;
}

type CounterAction =
    | { type: 'increment'; payload: number }
    | { type: 'decrement'; payload: number }
    | { type: 'reset' }
    | { type: 'setFlag'; payload: boolean };

const initialState: CounterState = {
    count: 0,
    flag: false
};

const counterReducer = (state: CounterState, action: CounterAction): CounterState => {
    switch (action.type) {
        case 'increment':
            return { ...state, count: state.count + action.payload };
        case 'decrement':
            return { ...state, count: state.count - action.payload };
        case 'reset':
            return { ...initialState };
        case 'setFlag':
            return { ...state, flag: action.payload };
        default:
            return state;
    }
}

const Counter: React.FC<CounterProps> = ({ interval = 1 }) => {
    const [state, dispatch] = useReducer(counterReducer, initialState);
    const clickCount = useRef(0);

    const manageClickCount = useCallback(() => {
        clickCount.current++;
        if (clickCount.current > 9) {
            dispatch({ type: 'setFlag', payload: true });
        }
    }, []);

    const inc = useCallback(() => {
        dispatch({ type: 'increment', payload: interval });
        manageClickCount();
    }, [interval, manageClickCount]);

    const dec = useCallback(() => {
        dispatch({ type: 'decrement', payload: interval });
        manageClickCount();
    }, [interval, manageClickCount])

    const reset = useCallback(() => {
        dispatch({ type: 'reset' });
        clickCount.current = 0;
    }, []);

    return (
        <>
            <div className="text-center">
                <h3 className="text-info">Counter Component Using Reducer</h3>
            </div>
            <div className="d-grid gap-2 mx-auto col-6">
                <input type="text" className="form-control form-control-lg" value={state.count} readOnly />
            </div>
            <ChildComponent inc={inc} dec={dec} reset={reset} flag={state.flag} />
        </>
    );
}

interface ChildComponentProps {
    inc: () => void;
    dec: () => void;
    reset: () => void;
    flag: boolean;
}

const ChildComponent: React.FC<ChildComponentProps> = React.memo(function ChildComponent({ inc, dec, reset, flag }) {
    console.log("Child Component is Rendered");

    return (
        <div className="d-grid gap-2 mx-auto col-6 mt-3">
            <button className="btn btn-info" disabled={flag} onClick={inc}>
                <span className='fs-4'>+</span>
            </button>
            <button className="btn btn-info" disabled={flag} onClick={dec}>
                <span className='fs-4'>-</span>
            </button>
            <button className="btn btn-secondary" disabled={!flag} onClick={reset}>
                <span className='fs-4'>Reset</span>
            </button>
        </div>
    );
});

const CounterAssignmentReducer: React.FC = () => (
    <div>
        <Counter />
    </div>
);

export default CounterAssignmentReducer;