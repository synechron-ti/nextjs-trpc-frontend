// 'use client'
// import React, { Component } from 'react';

// class Hello extends Component {
//     render() {
//         return (
//             <div className='orange'>
//                 <h1>Hello World</h1>
//             </div>
//         );
//     }
// }

// export default Hello;

import React from 'react';

const Hello = () => {
    return (
        <div className='orange'>
            <h1>Hello World</h1>
        </div>
    );
};

export default Hello;