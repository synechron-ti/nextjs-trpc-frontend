const Hello = () => {
    return (
        <div className="orange">
            <h1>Hello World</h1>
        </div>
    );
};

ReactDOM.render(<Hello />, document.getElementById('app'));