import { EmployeeSchema, employeeSchema } from "../shared/schemas/employee-schema";

const API_URL = 'http://localhost:8001/employees';

export const fetchEmployees = async () => {
    const response = await fetch(API_URL);
    if (!response.ok) {
        throw new Error('Failed to fetch employees');
    }
    const data = await response.json();
    return employeeSchema.array().parse(data);
}

export const fetchEmployeesSSR = async () => {
    const response = await fetch(API_URL, { cache: 'no-store' });
    if (!response.ok) {
        throw new Error('Failed to fetch employees');
    }
    const data = await response.json();
    return employeeSchema.array().parse(data);
}

export const createEmployee = async (employee: EmployeeSchema) => {
    employeeSchema.parse(employee);
    const response = await fetch(API_URL, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(employee)
    });

    if (!response.ok) {
        throw new Error('Failed to create employee');
    } else {
        const data = await response.json();
        return employeeSchema.parse(data);
    }
}

export const updateEmployee = async (employee: EmployeeSchema) => {
    employeeSchema.parse(employee);
    const response = await fetch(`${API_URL}/${employee.id}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(employee)
    });
    if (!response.ok) {
        throw new Error('Failed to update employee');
    } else {
        const data = await response.json();
        return employeeSchema.parse(data);
    }
}

export const deleteEmployee = async (id: number) => {
    const response = await fetch(`${API_URL}/${id}`, {
        method: 'DELETE'
    });
    if (!response.ok) {
        throw new Error('Failed to delete employee');
    } else {
        return id;
    }
}