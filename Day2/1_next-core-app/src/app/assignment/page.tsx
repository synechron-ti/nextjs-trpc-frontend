// import { Metadata } from "next";
// // import CrudRootComponent from "./using-custom-components/crud-root";
// // import CrudRootComponent from "./using-hook-form/crud-root";
// // import CrudRootComponent from "./using-hook-form-zod/crud-root";
// import CrudRootComponent from "./using-hook-form-zod-api-calls/crud-root";

// export const metadata: Metadata = {
//     title: "Next App | Assignment",
//     description: "This is the Assignment page of the Next App",
// };

// export default function Assignment() {
//     return (
//         <main className="container">
//             <h3 className="text-center text-primary">Welcome to the Employee Management System</h3>
//             <CrudRootComponent />
//         </main>
//     );
// }

// ---------------------------------------------------- For SSR

import { Metadata } from "next";
import CrudRootComponent from "./using-hook-form-zod-api-calls-ssr/crud-root";
import { fetchEmployeesSSR } from "../services/employee-service-client";

export const metadata: Metadata = {
    title: "Next App | Assignment",
    description: "This is the Assignment page of the Next App",
};

export default async function Assignment() {
    const employees = await fetchEmployeesSSR();
    return (
        <main className="container">
            <h3 className="text-center text-primary">Welcome to the Employee Management System</h3>
            <CrudRootComponent data={employees} />
        </main>
    );
}