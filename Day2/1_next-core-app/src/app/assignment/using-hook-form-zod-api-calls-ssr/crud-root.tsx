'use client'
import React, { useCallback, useEffect, useState } from 'react';
import DataTable from '@/app/_components/common/data-table';
import FormComponent from './form';
import ConfirmModal from '@/app/_components/common/confirm-model';
import { EmployeeSchema } from '@/app/shared/schemas/employee-schema';
import { createEmployee, deleteEmployee, fetchEmployees, updateEmployee } from '@/app/services/employee-service-client';

interface CrudRootComponentProps {
    data: EmployeeSchema[];
}

const CrudRootComponent: React.FC<CrudRootComponentProps> = ({ data }) => {
    const [employees, setEmployees] = useState<EmployeeSchema[]>(data);
    const [employee, setEmployee] = useState<EmployeeSchema>({ id: 1, name: "", designation: "", salary: 0 });
    const [edit, setEdit] = useState<boolean>(false);
    const [formDisabled, setFormDisabled] = useState<boolean>(false);
    const [showModal, setShowModal] = useState<boolean>(false);
    const [deleteCandidate, setDeleteCandidate] = useState<number | string | null>(null);

    // const loadEmployees = useCallback(async () => {
    //     try {
    //         const data = await fetchEmployees();
    //         setEmployees(data);
    //     } catch (err) {
    //         console.error('Error loading Employees');
    //     }
    // }, []);

    const getNextId = useCallback((employees: EmployeeSchema[]): number => {
        return employees.length ? Math.max(...employees.map((item) => item.id)) + 1 : 1;
    }, []);

    useEffect(() => {
        setEmployee({ id: getNextId(employees), name: "", designation: "", salary: 0 });
    }, [employees, getNextId]);

    const resetEmployee = useCallback(() => {
        setEmployee({ id: getNextId(employees), name: "", designation: "", salary: 0 });
        setEdit(false);
    }, [employees, getNextId]);

    const saveEmployee = useCallback(async (data: EmployeeSchema) => {
        try {
            if (edit) {
                const updatedEmployee = await updateEmployee(data);
                const index = employees.findIndex((item) => item.id === updatedEmployee.id);
                const updatedEmployees = [...employees];
                updatedEmployees[index] = updatedEmployee;
                setEmployees(updatedEmployees);
            } else {
                const newEmployee = await createEmployee(data);
                setEmployees([...employees, newEmployee]);
            }

            resetEmployee();
        } catch (error) {
            console.error('Error saving Employee');
        }
    }, [edit, employees, resetEmployee]);

    const selectEmployee = useCallback((item: EmployeeSchema, allowEdit: boolean) => {
        if (allowEdit) {
            setEmployee({ ...item });
            setEdit(true);
            setFormDisabled(false);
        } else {
            setEmployee({ ...item });
            setEdit(false);
            setFormDisabled(true);
        }
    }, []);

    const removeEmployee = useCallback((id: number | string) => {
        setShowModal(true);
        setDeleteCandidate(id);
    }, []);

    const handleModalYes = useCallback(async () => {
        if (deleteCandidate !== null) {
            try {
                const id = await deleteEmployee(deleteCandidate as number);
                const updatedEmployees = employees.filter((item) => item.id !== id);
                setEmployees(updatedEmployees);
                setDeleteCandidate(null);
                setShowModal(false);
            } catch (error) {
                console.error('Error deleting Employee');
            }
        }
    }, [deleteCandidate, employees]);

    const handleModalNo = useCallback(() => {
        setShowModal(false);
        setDeleteCandidate(null);
        setEmployee({ id: getNextId(employees), name: "", designation: "", salary: 0 });
    }, [employees, getNextId]);

    return (
        <div className='mt-3'>
            <FormComponent
                employee={employee}
                saveEmployee={saveEmployee}
                resetEmployee={resetEmployee}
                disabled={formDisabled} />
            <hr />
            <DataTable items={employees} onSelect={selectEmployee} onDelete={removeEmployee}>
                <h5 className="text-primary text-uppercase font-weight-bold">Employees Table</h5>
            </DataTable>
            <ConfirmModal show={showModal}
                title="Confirm Delete"
                message="Are you sure, you want to delete this record?"
                handleYes={handleModalYes}
                handleNo={handleModalNo} />
        </div>
    );
};

export default CrudRootComponent;