import React from 'react';

import Link from 'next/link';
import Image from "next/image";

import logo from '../../../images/logo.png';

const Navigation = () => {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <div className="container-fluid">
                <Link href="/" className="navbar-brand d-flex flex-column align-items-center">
                    <Image src={logo} alt="Logo" width={40} height={40} />
                    <span>TECHNIZER INDIA</span>
                </Link>
                <button
                    className="navbar-toggler"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#navbarNav"
                    aria-controls="navbarNav"
                    aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav ms-auto">
                        <li className="nav-item px-3">
                            <Link href="/home" className="nav-link d-flex flex-column align-items-center">
                                <i className="bi bi-house-fill"></i>
                                <span>Home</span>
                            </Link>
                        </li>
                        <li className="nav-item px-3">
                            <Link href="/hooks" className="nav-link d-flex flex-column align-items-center">
                                <i className="bi bi-substack"></i>
                                <span>Hooks</span>
                            </Link>
                        </li>
                        <li className="nav-item px-3">
                            <Link href="/settings" className="nav-link d-flex flex-column align-items-center">
                                <i className="bi bi-gear-fill"></i>
                                <span>Settings</span>
                            </Link>
                        </li>
                        <li className="nav-item px-3">
                            <Link href="/conference" className="nav-link d-flex flex-column align-items-center">
                                <i className="bi bi-people-fill"></i>
                                <span>Conference</span>
                            </Link>
                        </li>
                        <li className="nav-item px-3">
                            <Link href="/assignment" className="nav-link d-flex flex-column align-items-center">
                                <i className="bi bi-bookmark-fill"></i>
                                <span>Assignment</span>
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    );
};

export default Navigation;