import fastify from "fastify";
import { fastifyTRPCPlugin, FastifyTRPCPluginOptions } from "@trpc/server/adapters/fastify";
import { appRouter, type AppRouter } from "@/server/routers";
import { createContext } from "@/server/context";
import Next from "next";

const dev = process.env.NODE_ENV !== "production";
const nextApp = Next({dev});
const handle = nextApp.getRequestHandler();

const server = fastify({
    maxParamLength: 5000,
    logger: false
});

server.register(fastifyTRPCPlugin, {
    prefix: "/api/trpc",
    trpcOptions: {
        router: appRouter,
        createContext,
        onError({ path, error }) {
            // Report to Error Reporting Service
            console.error(`Error in tRPC handler on path '${path}': ${error}`);
        }
    } satisfies FastifyTRPCPluginOptions<AppRouter>['trpcOptions'],
});

nextApp.prepare().then(() => {
    server.get('/_next/*', (req, reply) => {
        handle(req.raw, reply.raw).then(()=>{
            reply.hijack();
        });
    });

    server.all('/*', (req, reply) => {
        handle(req.raw, reply.raw).then(()=>{
            reply.hijack();
        });
    });

    server.listen({ port: 3000 }, function (err, address) {
        if (err) {
            console.error(err);
            process.exit(1);
        }
        console.log("Server listening at http://localhost:3000");
    });
});