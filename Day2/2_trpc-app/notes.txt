Install Docker Desktop
docker pull postgres
docker run --name some-postgres -e POSTGRES_PASSWORD=P@ssword -d -p 5432:5432 postgres

npm i drizzle-orm pg --force
npm i -D @types/pg
npm i -D drizzle-kit

add the generate and migrate scripts in package.json

in src folder add db folder
add db/schema.ts

npm run generate
npm run migrate / npm run code-migrate (If success, tables will be created in DB)

Use DB from db/index.js to interact with database in Data Access Layer