import { Metadata } from "next";

export const metadata: Metadata = {
    title: "Next App | Settings",
    description: "This is the Settings page of the Next App",
};

export default function Settings() {
    return (
        <main className="text-center text-primary">
            <h3>This is the Settings Page</h3>
        </main>
    );
}