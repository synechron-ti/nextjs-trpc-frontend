import Image from "next/image";
import styles from "./home.module.css";
import { Metadata } from "next";

import bgImage from '../../images/home-image-1.jpg';

export const metadata: Metadata = {
    title: "Next App | Home",
    description: "This is the Home page of the Next App",
};

export default function Home() {
    return (
        <>
            <div className={styles.bgWrap}>
                <Image
                    src={bgImage}
                    alt="Picture for Background"
                    placeholder='blur'
                    quality={100}
                    sizes='100vw'
                    fill
                    style={{
                        objectFit: 'cover',
                    }}
                />
            </div>
            <h1 className={styles.bgHeader}>Charlotte Eriksson</h1>
            <p className={styles.bgText}>
                You can start a new at any given moment. <br />Life is just the passage of time and it’s up to you to pass it as you please.
            </p>
        </>
    )
}