import styles from "../conference.module.css";
import { Session } from "../models/session";

// SSR - Server Side Rendering
async function fetchSessions() {
    // We can access the Database here
    // We can access the API here
    // We can access the File System here
    const response = await fetch('http://localhost:8001/sessions', { cache: "no-store" });
    const data = await response.json() as Array<Session>;
    return data;
}

export default async function Page() {
    const sessions = await fetchSessions();

    return (
        <div className={styles.parentContainer}>
            <div>
                Last Rendered: {new Date().toLocaleTimeString()}
            </div>
            <h1>Welcome to Technizer India Sessions</h1>

            {
                sessions.map(
                    ({ id, title, description, room, day, track, speakers }) => (
                        <div key={id} className={styles.infoContainer}>
                            <h3 className={styles.titleText}>{title}</h3>
                            {speakers &&
                                speakers.map(({ id, name }) => (
                                    <h3 key={id} className={styles.titleText}>Speaker: {name}</h3>
                                ))}
                            <h5 className={styles.descText}>{description}</h5>
                            <h4 className={styles.infoText}>Room: {room}</h4>
                            <h4 className={styles.infoText}>Day: {day}</h4>
                            <h4 className={styles.infoText}>Track: {track}</h4>
                        </div>
                    )
                )
            }
        </div>
    )
}