import { Speaker } from "../models/speaker";
import styles from "../conference.module.css";
import Link from 'next/link';

// SSG - Static Site Generation
// async function fetchSpeakers() {
//     // We can access the Database here
//     // We can access the API here
//     // We can access the File System here
//     const response = await fetch('http://localhost:8001/speakers');
//     const data = await response.json() as Array<Speaker>;
//     return data;
// }

// ISR - Incremental Static Regeneration
async function fetchSpeakers() {
    // We can access the Database here
    // We can access the API here
    // We can access the File System here
    const response = await fetch('http://localhost:8001/speakers', { next: { revalidate: 10 } });
    const data = await response.json() as Array<Speaker>;
    return data;
}

export default async function Page() {
    const speakers = await fetchSpeakers();
    return (
        <div className={styles.parentContainer}>
            <div>
                Last Rendered: {new Date().toLocaleTimeString()}
            </div>
            <h1>Welcome to Technizer India Speakers</h1>
            {
                speakers.map(({ id, name, bio }) => (
                    <div key={id} className={styles.infoContainer}>
                        <Link
                            className={styles.bgLinks}
                            href={`/conference/speakers/${id}`}
                            prefetch={false}
                        >
                            <h3 className={styles.titleText}>{name}</h3>
                        </Link>
                        <h5 className={styles.descText}>{bio}</h5>
                    </div>
                ))
            }
        </div>
    )
}