import Link from "next/link";
import styles from "@/app/conference/conference.module.css";
import SpeakerInfo from "@/app/_components/speakers/speaker-info";

export default function Page({ params }: { params: { id: string } }) {
    return (
        <div className={styles.parentContainer}>
            <Link href="/conference/speakers">Back to Speakers</Link>
            <SpeakerInfo id={params.id} />
        </div>
    );
}