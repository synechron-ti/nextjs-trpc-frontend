import Image from "next/image";
import Link from "next/link";
import conferenceImage from "../../images/media-image-1.jpg";
import styles from "./conference.module.css";
import { Metadata } from "next";

export const metadata: Metadata = {
    title: "Next App | Conference",
    description: "This is the Conference page of the Next App",
};
export default function Conference() {
    return (
        <>
            <div className={styles.bgWrap}>
                <Image
                    src={conferenceImage}
                    alt="Conference Picture"
                    placeholder='blur'
                    quality={100}
                    sizes='100vw'
                    fill
                    style={{
                        objectFit: 'cover',
                    }}
                />
            </div>
            <h1 className={styles.bgHeader}>Welcome to Techinzer India Conference</h1>
            <h2 className={styles.bgText}>
                <Link className={styles.bgLinks} href="/conference/speakers">
                    View Speakers
                </Link>
            </h2>
            <h2 className={styles.bgText}>
                {/* <Link className={styles.bgLinks} href="/conference/sessions" prefetch={false}>
                    View Sessions
                </Link> */}
                <a className={styles.bgLinks} href="/conference/sessions">
                    View Sessions
                </a>
            </h2>
        </>
    )
}