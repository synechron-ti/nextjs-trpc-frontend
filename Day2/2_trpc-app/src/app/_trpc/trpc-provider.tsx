'use client'
import { QueryClientProvider, QueryClient } from "@tanstack/react-query";
import { trpc_client } from "./client";
import { useState } from "react";
import { httpBatchLink } from "@trpc/client";

export default function TrpcProvider({ children }: { children: React.ReactNode }) {
    const [queryClient] = useState(() => new QueryClient({}));
    const [trpcClient] = useState(() =>
        trpc_client.createClient({
            links: [
                httpBatchLink({
                    url: "http://localhost:3000/api/trpc"
                }),
            ]
        })
    );

    return (
        <trpc_client.Provider client={trpcClient} queryClient={queryClient}>
            <QueryClientProvider client={queryClient}>
                {children}
            </QueryClientProvider>
        </trpc_client.Provider>
    );
}