import Table from 'react-bootstrap/Table';
import { Metadata } from "next";
import CounterAssignment from '../_components/counter/counter-assignment';
import CounterAssignmentReducer from '../_components/counter/counter-assignment-reducer';
import CounterAssignmentSharedState from '../_components/counter/sharing-data';

export const metadata: Metadata = {
  title: "Next App | Hooks",
  description: "This is the Hooks page of the Next App",
};

export default function Index() {
  return (
    <main className="container text-center mt-3">
      {/* <CounterAssignment /> */}
      {/* <CounterAssignmentReducer /> */}
      <CounterAssignmentSharedState />
    </main>
  );
}
