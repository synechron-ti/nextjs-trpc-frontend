'use client'
import React, { useCallback, useEffect, useState } from 'react';
import DataTable from '@/app/_components/common/data-table';
import FormComponent from './form';
import ConfirmModal from '@/app/_components/common/confirm-model';
import { SelectEmployeeSchema } from '@/shared/schemas/employee-schema';
import { trpc_client } from '@/app/_trpc/client';

const getNextId = (employees: SelectEmployeeSchema[] | undefined): number => {
    if (!employees) return 1;
    return employees.length ? Math.max(...employees.map(e => e.id)) + 1 : 1;
};

const CrudRootComponent: React.FC = () => {
    const [fetchEmployeesTriggered, setFetchEmployeesTriggered] = useState<boolean>(false);

    const { data: employees, refetch: fetchEmployees } = trpc_client.getAllEmployees.useQuery(undefined, {
        enabled: fetchEmployeesTriggered,
        refetchOnWindowFocus: false,
    });

    const addEmployee = trpc_client.addEmployee.useMutation();
    const updateEmployee = trpc_client.updateEmployee.useMutation();
    const deleteEmployee = trpc_client.deleteEmployee.useMutation();

    const [employee, setEmployee] = useState<SelectEmployeeSchema>({ id: 1, name: "", designation: "", salary: 0 });
    const [edit, setEdit] = useState<boolean>(false);
    const [formDisabled, setFormDisabled] = useState<boolean>(false);
    const [showModal, setShowModal] = useState<boolean>(false);
    const [deleteCandidate, setDeleteCandidate] = useState<number | string | null>(null);

    const loadEmployees = useCallback(async () => {
        if (!fetchEmployeesTriggered) {
            setFetchEmployeesTriggered(true);
        } else {
            fetchEmployees();
        }
    }, [fetchEmployeesTriggered, fetchEmployees]);

    useEffect(() => {
        if (!fetchEmployeesTriggered) {
            loadEmployees();
        }
    }, [fetchEmployeesTriggered, loadEmployees]);

    useEffect(() => {
        setEmployee({ id: getNextId(employees), name: "", designation: "", salary: 0 });
    }, [employees]);

    const resetEmployee = useCallback(() => {
        setEmployee({ id: getNextId(employees), name: "", designation: "", salary: 0 });
        setEdit(false);
    }, [employees]);

    const saveEmployee = useCallback(async (data: SelectEmployeeSchema) => {
        try {
            if (edit) {
                await updateEmployee.mutateAsync(data);
            } else {
                await addEmployee.mutateAsync(data);
            }
            loadEmployees();
            resetEmployee();
        } catch (error) {
            console.error('Error saving Employee');
        }
    }, [edit, loadEmployees, resetEmployee, addEmployee, updateEmployee]);

    const selectEmployee = useCallback((item: SelectEmployeeSchema, allowEdit: boolean) => {
        if (allowEdit) {
            setEmployee({ ...item });
            setEdit(true);
            setFormDisabled(false);
        } else {
            setEmployee({ ...item });
            setEdit(false);
            setFormDisabled(true);
        }
    }, []);

    const removeEmployee = useCallback((id: number | string) => {
        setShowModal(true);
        setDeleteCandidate(id);
    }, []);

    const handleModalYes = useCallback(async () => {
        if (deleteCandidate !== null) {
            try {
                await deleteEmployee.mutateAsync({ id: Number(deleteCandidate) });
                loadEmployees();
                setDeleteCandidate(null);
                setShowModal(false);
            } catch (error) {
                console.error('Error deleting Employee');
            }
        }
    }, [deleteCandidate, loadEmployees, deleteEmployee]);

    const handleModalNo = useCallback(() => {
        setShowModal(false);
        setDeleteCandidate(null);
        setEmployee({ id: getNextId(employees), name: "", designation: "", salary: 0 });
    }, [employees]);

    return (
        <div className='mt-3'>
            <FormComponent
                employee={employee}
                saveEmployee={saveEmployee}
                resetEmployee={resetEmployee}
                disabled={formDisabled} />
            <hr />
            <DataTable items={employees} onSelect={selectEmployee} onDelete={removeEmployee}>
                <h5 className="text-primary text-uppercase font-weight-bold">Employees Table</h5>
            </DataTable>
            <ConfirmModal show={showModal}
                title="Confirm Delete"
                message="Are you sure, you want to delete this record?"
                handleYes={handleModalYes}
                handleNo={handleModalNo} />
        </div>
    );
};

export default CrudRootComponent;