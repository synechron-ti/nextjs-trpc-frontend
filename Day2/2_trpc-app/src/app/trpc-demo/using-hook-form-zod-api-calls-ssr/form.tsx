import TextInputHookForm from '@/app/_components/common/text-input-hook-form-improved';
import { selectEmployeeSchema, SelectEmployeeSchema } from '@/shared/schemas/employee-schema';
import { zodResolver } from '@hookform/resolvers/zod';
import React, { useEffect } from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';

interface FormComponentProps {
    employee: SelectEmployeeSchema;
    disabled: boolean;
    saveEmployee: (data: SelectEmployeeSchema) => void;
    resetEmployee: () => void;
}

const FormComponent: React.FC<FormComponentProps> = ({ employee, saveEmployee, resetEmployee, disabled }) => {
    const { register, handleSubmit, reset, formState: { errors } } = useForm<SelectEmployeeSchema>({
        resolver: zodResolver(selectEmployeeSchema),
        defaultValues: employee
    });

    useEffect(() => {
        reset(employee);
    }, [employee, reset]);

    const onSubmit: SubmitHandler<SelectEmployeeSchema> = (data) => {
        saveEmployee(data);
    }

    return (
        <div className='row'>
            <div className='col-sm-8 offset-sm-2'>
                <form className='form-horizontal' autoComplete='off' onSubmit={handleSubmit(onSubmit)} noValidate>
                    <fieldset disabled={disabled}>
                        <legend className="text-center text-secondary text-uppercase font-weight-bold">Add/Edit Employee Information</legend>
                        <hr className="mt-0" />

                        <div className="row mb-3">
                            <div className="col-md-6">
                                <TextInputHookForm label="Employee ID" type="number" name="id" readOnly={true} register={register} errors={errors} />
                            </div>
                            <div className="col-md-6">
                                <TextInputHookForm label="Employee Name" name="name" register={register} errors={errors} />
                            </div>
                        </div>
                        <div className="row mb-3">
                            <div className="col-md-6">
                                <TextInputHookForm label="Designation" name="designation" register={register} errors={errors} />
                            </div>
                            <div className="col-md-6">
                                <TextInputHookForm label="Salary" type="number" name="salary" register={register} errors={errors} />
                            </div>
                        </div>
                        <div className="row mt-3">
                            <div className="col-md-6">
                                <button type="submit" className="btn btn-success w-100">Save</button>
                            </div>
                            <div className="col-md-6">
                                <button type="reset" className="btn btn-primary w-100" onClick={resetEmployee}>Reset</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    );
};

export default FormComponent;