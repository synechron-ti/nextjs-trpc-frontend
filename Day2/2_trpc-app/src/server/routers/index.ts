import { t } from "../utils/trpc";
import { employeesRouter } from "./employees-router";

export const appRouter = t.mergeRouters(
    employeesRouter
);

export type AppRouter = typeof appRouter;