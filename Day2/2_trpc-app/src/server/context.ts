import { CreateFastifyContextOptions } from "@trpc/server/adapters/fastify";

export async function createContext({ req, res }: CreateFastifyContextOptions) {
    const user = undefined;
    return {
        req,
        res,
        user
    };
}

export type Context = Awaited<ReturnType<typeof createContext>>;