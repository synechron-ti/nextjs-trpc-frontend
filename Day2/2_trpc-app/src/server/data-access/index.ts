import { db } from "@/db";
import { employeesTable } from "@/db/schema";
import { selectEmployeeSchema, SelectEmployeeSchema, insertEmployeeSchema, InsertEmployeeSchema, UpdateEmployeeSchema, updateEmployeeSchema } from "@/shared/schemas/employee-schema";
import { eq } from "drizzle-orm";

export const getEmployees = async (): Promise<SelectEmployeeSchema[]> => {
    const result = await db.select().from(employeesTable);
    return result.map(row => selectEmployeeSchema.parse(row));
};

const getEmployee = (id: number) => {
    const employee = db.select().from(employeesTable).where(eq(employeesTable.id, id)).execute();
    return employee;
}

const insertEmployee = async (employeeToInsert: InsertEmployeeSchema) => {
    const validEmployee = insertEmployeeSchema.parse(employeeToInsert);  // Validate employee data before inserting
    await db.insert(employeesTable).values(validEmployee).execute();
}

const findAndUpdateEmployee = async (id: number, employeeToUpdate: UpdateEmployeeSchema) => {
    const validEmployee = updateEmployeeSchema.parse(employeeToUpdate);  // Validate employee data before updating
    await db.update(employeesTable).set(validEmployee).where(eq(employeesTable.id, id)).execute();
}

const findAndDeleteEmployee = async (id: number) => {
    await db.delete(employeesTable).where(eq(employeesTable.id, id)).execute();
}

export const employeesDAO = {
    getEmployees,
    getEmployee,
    insertEmployee,
    findAndUpdateEmployee,
    findAndDeleteEmployee
};