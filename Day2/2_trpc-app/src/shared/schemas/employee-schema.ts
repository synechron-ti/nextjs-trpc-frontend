import { employeesTable } from "@/db/schema";
import { createInsertSchema, createSelectSchema } from "drizzle-zod";
import { z } from 'zod';

export const insertEmployeeSchema = createInsertSchema(employeesTable).extend({
    salary: z.coerce.number().positive({ message: 'Salary must be greater than zero' }).nonnegative({ message: 'Salary must be a number' }),
});

export const updateEmployeeSchema = createInsertSchema(employeesTable).extend({
    id: z.coerce.number().nonnegative().int(),
    salary: z.coerce.number().positive({ message: 'Salary must be greater than zero' }).nonnegative({ message: 'Salary must be a number' }),
});

export const deleteEmployeeSchema = z.object({
    id: z.coerce.number().nonnegative().int(),
});

export const selectEmployeeSchema = createSelectSchema(employeesTable).extend({
    name: z.string().min(1, { message: 'Employee Name is required' }),
    designation: z.string().min(1, { message: 'Designation is required' }),
    salary: z.coerce.number().positive({ message: 'Salary must be greater than zero' }).nonnegative({ message: 'Salary must be a number' }),
});

export type SelectEmployeeSchema = z.infer<typeof selectEmployeeSchema>;
export type InsertEmployeeSchema = z.infer<typeof insertEmployeeSchema>;
export type UpdateEmployeeSchema = z.infer<typeof updateEmployeeSchema>;
export type DeleteEmployeeSchema = z.infer<typeof deleteEmployeeSchema>;