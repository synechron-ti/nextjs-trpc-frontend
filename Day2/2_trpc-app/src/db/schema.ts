import { pgTable, serial, text, integer } from "drizzle-orm/pg-core";

export const employeesTable = pgTable("employees", {
    id: serial("id").primaryKey(),
    name: text("name").notNull(),
    designation: text("designation").notNull(),
    salary: integer("salary").notNull(),  // Store salary as a numeric field
});