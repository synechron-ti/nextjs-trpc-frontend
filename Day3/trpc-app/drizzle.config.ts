import { defineConfig } from 'drizzle-kit';

export default defineConfig({
    dialect: "postgresql",
    schema: "./src/db/schema.ts",
    out: "./src/db/migrations",
    dbCredentials: {
        user: process.env.DB_USER || "postgres",
        host: process.env.DB_HOST || "localhost",
        database: process.env.DB_NAME || "postgres",
        password: process.env.DB_PASS || "P@ssword",
        port: Number(process.env.DB_PORT) || 5432,
        ssl: false
    }
});