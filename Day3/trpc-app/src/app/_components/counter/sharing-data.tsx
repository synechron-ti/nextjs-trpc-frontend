'use client'

// ------------------------------------------------- Using Context

import React, { createContext, useCallback, useContext, useReducer, useRef } from 'react';

interface CounterProps {
    interval?: number;
}

interface CounterState {
    count: number;
    flag: boolean;
}

// Action Creators
const increment = (payload: number) => ({ type: 'increment', payload } as const);
const decrement = (payload: number) => ({ type: 'decrement', payload } as const);
const resetCounter = () => ({ type: 'reset', } as const);
const setFlag = (payload: boolean) => ({ type: 'setFlag', payload } as const);

type CounterAction =
    | ReturnType<typeof increment>
    | ReturnType<typeof decrement>
    | ReturnType<typeof resetCounter>
    | ReturnType<typeof setFlag>;

const initialState: CounterState = {
    count: 0,
    flag: false
};

const counterReducer = (state: CounterState, action: CounterAction): CounterState => {
    switch (action.type) {
        case 'increment':
            return { ...state, count: state.count + action.payload };
        case 'decrement':
            return { ...state, count: state.count - action.payload };
        case 'reset':
            return { ...initialState };
        case 'setFlag':
            return { ...state, flag: action.payload };
        default:
            return state;
    }
}

// 1. Context
const CounterContext = createContext<{
    state: CounterState;
    dispatch: React.Dispatch<CounterAction>;
} | undefined>(undefined);

// 2. Provider Component
const CounterProvider: React.FC<{ children: React.ReactNode }> = ({ children }) => {
    const [state, dispatch] = useReducer(counterReducer, initialState);

    return (
        <CounterContext.Provider value={{ state, dispatch }}>
            {children}
        </CounterContext.Provider>
    );
}

// 3. Custom Hook
const useCounter = () => {
    const context = useContext(CounterContext);
    if (context === undefined) {
        throw new Error('useCounter must be used within a CounterProvider');
    }
    return context;
}

const Counter: React.FC<CounterProps> = ({ interval = 1 }) => {
    const { state, dispatch } = useCounter();
    const clickCount = useRef(0);

    const manageClickCount = useCallback(() => {
        clickCount.current++;
        if (clickCount.current > 9) {
            dispatch(setFlag(true));
        }
    }, [dispatch]);

    const inc = useCallback(() => {
        dispatch(increment(interval));
        manageClickCount();
    }, [interval, manageClickCount, dispatch]);

    const dec = useCallback(() => {
        dispatch(decrement(interval));
        manageClickCount();
    }, [interval, manageClickCount, dispatch])

    const reset = useCallback(() => {
        dispatch(resetCounter());
        clickCount.current = 0;
    }, [dispatch]);

    return (
        <>
            <div className="text-center">
                <h3 className="text-info">Counter Component Sharing Data</h3>
            </div>
            <div className="d-grid gap-2 mx-auto col-6">
                <input type="text" className="form-control form-control-lg" value={state.count} readOnly />
            </div>
            <ChildComponent inc={inc} dec={dec} reset={reset} flag={state.flag} />
        </>
    );
}

const CounterSibling: React.FC<CounterProps> = ({ interval = 1 }) => {
    const { state, dispatch } = useCounter();
    const clickCount = useRef(0);

    const inc = useCallback(() => {
        dispatch(increment(interval));
    }, [interval, dispatch]);

    const dec = useCallback(() => {
        dispatch(decrement(interval));
    }, [interval, dispatch])

    const reset = useCallback(() => {
        dispatch(resetCounter());
        clickCount.current = 0;
    }, [dispatch]);

    return (
        <>
            <div className="text-center">
                <h3 className="text-success">Counter Sibling Component Sharing Data</h3>
            </div>
            <div className="d-grid gap-2 mx-auto col-6">
                <input type="text" className="form-control form-control-lg" value={state.count} readOnly />
            </div>
            <ChildComponent inc={inc} dec={dec} reset={reset} flag={state.flag} />
        </>
    );
}

interface ChildComponentProps {
    inc: () => void;
    dec: () => void;
    reset: () => void;
    flag: boolean;
}

const ChildComponent: React.FC<ChildComponentProps> = React.memo(function ChildComponent({ inc, dec, reset, flag }) {
    return (
        <div className="d-grid gap-2 mx-auto col-6 mt-3">
            <button className="btn btn-info" disabled={flag} onClick={inc}>
                <span className='fs-4'>+</span>
            </button>
            <button className="btn btn-info" disabled={flag} onClick={dec}>
                <span className='fs-4'>-</span>
            </button>
            <button className="btn btn-secondary" disabled={!flag} onClick={reset}>
                <span className='fs-4'>Reset</span>
            </button>
        </div>
    );
});

const CounterAssignmentSharedState: React.FC = () => (
    <CounterProvider>
        <h1 className="text-primary">useContext to share Reducer State in every Component</h1>
        <Counter />
        <hr />
        <CounterSibling />
    </CounterProvider>
);

export default CounterAssignmentSharedState;

// // ------------------------------------------------- Using useReducer

// import React, { useCallback, useReducer, useRef } from 'react';

// interface CounterProps {
//     interval?: number;
// }

// interface CounterState {
//     count: number;
//     flag: boolean;
// }

// // Action Creators
// const increment = (payload: number) => ({ type: 'increment', payload } as const);
// const decrement = (payload: number) => ({ type: 'decrement', payload } as const);
// const resetCounter = () => ({ type: 'reset', } as const);
// const setFlag = (payload: boolean) => ({ type: 'setFlag', payload } as const);

// type CounterAction =
//     | ReturnType<typeof increment>
//     | ReturnType<typeof decrement>
//     | ReturnType<typeof resetCounter>
//     | ReturnType<typeof setFlag>;

// const initialState: CounterState = {
//     count: 0,
//     flag: false
// };

// const counterReducer = (state: CounterState, action: CounterAction): CounterState => {
//     switch (action.type) {
//         case 'increment':
//             return { ...state, count: state.count + action.payload };
//         case 'decrement':
//             return { ...state, count: state.count - action.payload };
//         case 'reset':
//             return { ...initialState };
//         case 'setFlag':
//             return { ...state, flag: action.payload };
//         default:
//             return state;
//     }
// }

// const Counter: React.FC<CounterProps> = ({ interval = 1 }) => {
//     const [state, dispatch] = useReducer(counterReducer, initialState);
//     const clickCount = useRef(0);

//     const manageClickCount = useCallback(() => {
//         clickCount.current++;
//         if (clickCount.current > 9) {
//             dispatch(setFlag(true));
//         }
//     }, []);

//     const inc = useCallback(() => {
//         dispatch(increment(interval));
//         manageClickCount();
//     }, [interval, manageClickCount]);

//     const dec = useCallback(() => {
//         dispatch(decrement(interval));
//         manageClickCount();
//     }, [interval, manageClickCount])

//     const reset = useCallback(() => {
//         dispatch(resetCounter());
//         clickCount.current = 0;
//     }, []);

//     return (
//         <>
//             <div className="text-center">
//                 <h3 className="text-info">Counter Component Sharing Data</h3>
//             </div>
//             <div className="d-grid gap-2 mx-auto col-6">
//                 <input type="text" className="form-control form-control-lg" value={state.count} readOnly />
//             </div>
//             <ChildComponent inc={inc} dec={dec} reset={reset} flag={state.flag} />
//         </>
//     );
// }

// const CounterSibling: React.FC<CounterProps> = ({ interval = 1 }) => {
//     const [state, dispatch] = useReducer(counterReducer, initialState);
//     const clickCount = useRef(0);

//     const manageClickCount = useCallback(() => {
//         clickCount.current++;
//         if (clickCount.current > 5) {
//             dispatch(setFlag(true));
//         }
//     }, []);

//     const inc = useCallback(() => {
//         dispatch(increment(interval));
//         manageClickCount();
//     }, [interval, manageClickCount]);

//     const dec = useCallback(() => {
//         dispatch(decrement(interval));
//         manageClickCount();
//     }, [interval, manageClickCount])

//     const reset = useCallback(() => {
//         dispatch(resetCounter());
//         clickCount.current = 0;
//     }, []);

//     return (
//         <>
//             <div className="text-center">
//                 <h3 className="text-success">Counter Sibling Component Sharing Data</h3>
//             </div>
//             <div className="d-grid gap-2 mx-auto col-6">
//                 <input type="text" className="form-control form-control-lg" value={state.count} readOnly />
//             </div>
//             <ChildComponent inc={inc} dec={dec} reset={reset} flag={state.flag} />
//         </>
//     );
// }

// interface ChildComponentProps {
//     inc: () => void;
//     dec: () => void;
//     reset: () => void;
//     flag: boolean;
// }

// const ChildComponent: React.FC<ChildComponentProps> = React.memo(function ChildComponent({ inc, dec, reset, flag }) {
//     console.log("Child Component is Rendered");

//     return (
//         <div className="d-grid gap-2 mx-auto col-6 mt-3">
//             <button className="btn btn-info" disabled={flag} onClick={inc}>
//                 <span className='fs-4'>+</span>
//             </button>
//             <button className="btn btn-info" disabled={flag} onClick={dec}>
//                 <span className='fs-4'>-</span>
//             </button>
//             <button className="btn btn-secondary" disabled={!flag} onClick={reset}>
//                 <span className='fs-4'>Reset</span>
//             </button>
//         </div>
//     );
// });

// const CounterAssignmentSharedState: React.FC = () => (
//     <div>
//         <h1 className="text-primary">useReducer State is diferrent for every Component</h1>
//         <Counter />
//         <hr />
//         <CounterSibling />
//     </div>
// );

// export default CounterAssignmentSharedState;

