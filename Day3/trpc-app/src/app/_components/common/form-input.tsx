import React from 'react';
import { useFormContext } from 'react-hook-form';
import { Form } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

type FormInputProps = {
  label: string;
  name: string;
  type?: string;
};

const FormInput: React.FC<FormInputProps> = ({
  label,
  name,
  type = 'text',
}) => {
  const {
    register,
    formState: { errors },
  } = useFormContext();

  return (
    <Form.Group className="mb-3">
      <Form.Label htmlFor={name} className="text-primary">
        {label}
      </Form.Label>
      <Form.Control
        id={name}
        type={type}
        placeholder=" "
        isInvalid={!!errors[name]}
        {...register(name)}
      />
      {errors[name] && (
        <Form.Control.Feedback type="invalid">
          {errors[name]?.message as string}
        </Form.Control.Feedback>
      )}
    </Form.Group>
  );
};

export default FormInput;
