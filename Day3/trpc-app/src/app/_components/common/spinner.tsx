import React from 'react';
import { Spinner as BootstrapSpinner } from 'react-bootstrap';

type SpinnerProps = {
    width?: string;
    height?: string;
    color?: string;
    bgColor?: string;
};

const Spinner: React.FC<SpinnerProps> = ({
    width = '1.25rem',
    height = '1.25rem',
    color,
    bgColor,
}) => {
    const spinnerStyle = {
        width,
        height,
        color,
        backgroundColor: bgColor,
    };

    return (
        <BootstrapSpinner
            animation="border"
            role="status"
            style={spinnerStyle}
            variant={color}
        >
            <span className="visually-hidden">Loading...</span>
        </BootstrapSpinner>
    );
};

export default Spinner;
