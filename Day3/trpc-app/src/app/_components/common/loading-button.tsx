import React from 'react';
import { Button, Spinner as BootstrapSpinner } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

type LoadingButtonProps = {
    loading: boolean;
    btnColor?: string;
    textColor?: string;
    children: React.ReactNode;
};

const LoadingButton: React.FC<LoadingButtonProps> = ({
    textColor = 'text-white',
    btnColor = 'primary',
    children,
    loading = false,
}) => {
    return (
        <Button
            type="submit"
            variant={btnColor}
            className={`w-100 py-3 font-semibold rounded-lg d-flex justify-content-center ${loading ? 'bg-secondary' : ''}`}
            disabled={loading}
        >
            {loading ? (
                <div className="d-flex align-items-center gap-3">
                    <BootstrapSpinner
                        as="span"
                        animation="border"
                        size="sm"
                        role="status"
                        aria-hidden="true"
                    />
                    <span className="text-slate-500">Loading...</span>
                </div>
            ) : (
                <span className={textColor}>{children}</span>
            )}
        </Button>
    );
};

export default LoadingButton;
