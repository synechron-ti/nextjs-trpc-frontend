import React from 'react';
import { FieldErrors, UseFormRegister } from 'react-hook-form';

interface TextInputHookFormProps {
    name: string;
    label: string;
    placeholder?: string;
    readOnly?: boolean;
    register: UseFormRegister<any>;
    errors?: FieldErrors;
    validation?: any;
}

const TextInputHookForm: React.FC<TextInputHookFormProps> = ({ name, label, placeholder, readOnly, register, errors, validation }) => {
    const error = errors && errors[name];
    const errorMessage = error && 'message' in error ? error.message : null;

    return (
        <div className="form-group mb-1">
            <label className="mb-0" htmlFor={name}>{label}</label>
            <input
                type="text"
                className={`form-control ${errorMessage ? 'is-invalid' : ''}`}
                id={name}
                placeholder={placeholder}
                readOnly={readOnly}
                {...register(name, validation)}
            />
            {errorMessage && <div className="invalid-feedback">{errorMessage.toString()}</div>}
        </div>
    );
};

export default TextInputHookForm;
