import React from 'react';

interface TextInputProps {
    name: string;
    label: string;
    placeholder?: string;
    value: string;
    readOnly?: boolean;
    onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const TextInput: React.FC<TextInputProps> = ({ name, label, placeholder, value, readOnly, onChange }) => {
    return (
        <div className="form-group mb-1">
            <label className="mb-0" htmlFor={name}>{label}</label>
            <input
                type="text"
                className="form-control"
                id={name}
                name={name}
                placeholder={placeholder}
                value={value}
                readOnly={readOnly}
                onChange={onChange}
            />
        </div>
    );
};

export default TextInput;
