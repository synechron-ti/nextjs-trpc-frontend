import Table from 'react-bootstrap/Table';
import { Metadata } from "next";
export const metadata: Metadata = {
  title: "Next App | Index",
  description: "This is the Index page of the Next App",
};

export default function Index() {
  // throw new Error("This is an intentional error");
  return (
    <main className="container text-center mt-3">
      <h3 className="text-primary">This is the Index Page</h3>
      <h3 className="text-success">
        <span className="bi bi-check-circle-fill">&nbsp;This is a Server Rendered Page</span>
        &nbsp;<i className="bi bi-emoji-smile"></i>
      </h3>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Username</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
          </tr>
          <tr>
            <td>2</td>
            <td>Jacob</td>
            <td>Thornton</td>
            <td>@fat</td>
          </tr>
          <tr>
            <td>3</td>
            <td colSpan={2}>Larry the Bird</td>
            <td>@twitter</td>
          </tr>
        </tbody>
      </Table>
    </main>
  );
}
