'use client'

export default function Error({ error, reset }: { error: Error & { digest?: string }, reset: () => void }) {
    return (
        <html>
            <body>
                <h2>From Global Error!</h2>
                <h2>Something went wrong!</h2>
                <button onClick={() => reset()}>Try Again</button>
            </body>
        </html>
    );
}