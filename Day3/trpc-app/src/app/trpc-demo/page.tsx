import { Metadata } from "next";
import CrudRootComponent from "./using-hook-form-zod-api-calls/crud-root";

export const metadata: Metadata = {
    title: "Next App | TRPC",
    description: "This is the TRPC page of the Next App",
};

export default function Assignment() {
    return (
        <main className="container">
            <h3 className="text-center text-primary">Welcome to the Employee Management System</h3>
            <CrudRootComponent />
        </main>
    );
}

// // ---------------------------------------------------------- Using SSR

// import { Metadata } from "next";
// import CrudRootComponent from "./using-hook-form-zod-api-calls-ssr/crud-root";
// import { serverClient } from "../_trpc/server-client";

// export const metadata: Metadata = {
//     title: "Next App | TRPC",
//     description: "This is the TRPC page of the Next App",
// };

// export default async function TrpcDemo() {
//     const data = await serverClient.getAllEmployees();
//     return (
//         <main className="container">
//             <h3 className="text-center text-primary">Welcome to the Employee Management System</h3>
//             <CrudRootComponent pemployees={data} />
//         </main>
//     );
// }