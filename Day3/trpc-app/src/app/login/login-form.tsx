'use client'
import { loginUserSchema, LoginUserSchema } from '@/shared/schemas/user-schema';
import { zodResolver } from '@hookform/resolvers/zod';
import { useRouter } from 'next/navigation';
import React from 'react';
import { FormProvider, SubmitHandler, useForm } from 'react-hook-form';
import { trpc_client } from '../_trpc/client';
import toast from 'react-hot-toast';
import FormInput from '../_components/common/form-input';
import LoadingButton from '../_components/common/loading-button';

const LoginForm = () => {
    const [submitting, setSubmitting] = React.useState(false);
    const router = useRouter();

    const methods = useForm<LoginUserSchema>({
        resolver: zodResolver(loginUserSchema)
    });

    const { reset, handleSubmit } = methods;

    const { mutate: loginFn } = trpc_client.loginUser.useMutation({
        onSettled() {
            setSubmitting(false);
        },
        onMutate() {
            setSubmitting(true);
        },
        onError(error: any) {
            toast.error(error.message);
            reset({ password: '' });
        },
        onSuccess(data: any) {
            const { token } = data;
            localStorage.setItem('app-access-token', token);
            toast.success('Logged in successfully');
            reset();
            router.push('/');
        }
    });

    const onSubmitHandler: SubmitHandler<LoginUserSchema> = (values) => {
        loginFn(values);
    }

    return (
        <FormProvider {...methods}>
            <form noValidate onSubmit={handleSubmit(onSubmitHandler)}>
                <FormInput name="email" label="Email" type="email" />
                <FormInput name="password" label="Password" type="password" />
                <LoadingButton loading={submitting}>
                    Login
                </LoadingButton>
            </form>
        </FormProvider>
    );
};

export default LoginForm;