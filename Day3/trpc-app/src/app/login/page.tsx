import { Metadata } from "next";
import LoginForm from "./login-form";

export const metadata: Metadata = {
    title: "Next App | Login",
    description: "This is the Login page of the Next App",
};

export default function Login() {
    return (
        <main>
            <div className="text-center">
                <h2 className="mb-4">Welcome Back</h2>
                <h3 className="mb-4">Login to get access</h3>
            </div>
            <div className="container">
                <LoginForm />
            </div>
        </main>
    );
}