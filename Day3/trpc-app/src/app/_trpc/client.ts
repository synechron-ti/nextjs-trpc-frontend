import { createTRPCReact } from "@trpc/react-query";
import { type AppRouter } from "@/server/routers";
export const trpc_client = createTRPCReact<AppRouter>({});