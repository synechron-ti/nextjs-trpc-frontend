import { appRouter } from "@/server/routers";
import { t } from "@/server/utils/trpc";
import { Context } from "@/server/context";

export const createCaller = t.createCallerFactory(appRouter);
export const serverClient = createCaller({} as Context);