import { migrate } from "drizzle-orm/node-postgres/migrator";
import { db } from './index';
import path from "path";

migrate(db, { migrationsFolder: path.join(process.cwd(), "src", "db", "migrations") })
    .then(() => {
        console.log("Migration complete!");
        process.exit(0);
    })
    .catch((err) => {
        console.error("Migrations failed!", err);
        process.exit(1);
    });