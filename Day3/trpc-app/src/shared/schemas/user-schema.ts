import {TypeOf, z} from 'zod';

export const loginUserSchema = z.object({
    email: z.string({ required_error: 'Email is required' }).min(1, 'Email is required').email({ message: 'Invalid email' }),
    password: z.string({ required_error: 'Password is required' }).min(1, 'Password is required'),
});

export type LoginUserSchema = TypeOf<typeof loginUserSchema>;