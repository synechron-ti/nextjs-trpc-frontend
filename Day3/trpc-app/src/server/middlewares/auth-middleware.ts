import { TRPCError } from "@trpc/server";
import { FastifyReply, FastifyRequest } from "fastify";
import jwt from 'jsonwebtoken';

export const deserializeUser = async (req: FastifyRequest, res: FastifyReply) => {
    try {
        let token;

        if (req.cookies['app-access-token']) {
            token = req.cookies['app-access-token'];
        }

        // let token = req.headers.authorization?.split(' ')[1];

        const notAuthenticated = {
            user: null
        }

        if (!token) {
            return notAuthenticated;
        }

        // Verify the token
        const secret = process.env.JWT_SECRET_KEY!;
        const decoded = jwt.verify(token, secret) as { sub: string };

        if (!decoded) {
            return notAuthenticated;
        }

        // Code to check , if the user still exists in the database
        // Get the user from the database based on the decoded.sub
        const user = {
            id: '1',
            name: 'Manish',
            email: 'manish@abc.com',
            password: 'Manish@123'
        };

        if (!user) {
            return notAuthenticated;
        }

        const { password, ...userWithoutPassword } = user;
        return {
            user: userWithoutPassword
        };
    }
    catch (err: any) {
        throw new TRPCError({
            code: 'INTERNAL_SERVER_ERROR',
            message: err.message
        })
    }
}