import { CreateFastifyContextOptions } from "@trpc/server/adapters/fastify";
import { deserializeUser } from "./middlewares/auth-middleware";

export async function createContext({ req, res }: CreateFastifyContextOptions) {
    const { user } = await deserializeUser(req, res);
    return {
        req,
        res,
        user
    };
}

export type Context = Awaited<ReturnType<typeof createContext>>;