import { loginUserSchema } from "@/shared/schemas/user-schema";
import { publicProcedure, router } from "../utils/trpc";
import { loginHandler } from "../controllers/account-controller";

export const accountRouter = router({
    loginUser: publicProcedure.input(loginUserSchema).mutation(({ input, ctx }) => {
        return loginHandler(input, ctx.res);
    })
});