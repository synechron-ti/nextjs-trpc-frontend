import { t } from "../utils/trpc";
import { accountRouter } from "./account-router";
import { employeesRouter } from "./employees-router";

export const appRouter = t.mergeRouters(
    employeesRouter,
    accountRouter
);

export type AppRouter = typeof appRouter;