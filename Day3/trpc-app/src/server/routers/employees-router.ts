import { addEmployee, deleteEmployee, getEmployee, getEmployees, updateEmployee } from "../controllers/employees-controller";
import { publicProcedure, router, securedProcedure } from "../utils/trpc";
import { z } from "zod";
import { deleteEmployeeSchema, insertEmployeeSchema, updateEmployeeSchema } from "@/shared/schemas/employee-schema";

const EmployeeIdSchema = z.number();

// export const employeesRouter = router({
//     getAllEmployees: publicProcedure.query(() => getEmployees()),
//     getEmployee: publicProcedure.input(EmployeeIdSchema).query(({ input }) => getEmployee(input)),
//     addEmployee: publicProcedure.input(insertEmployeeSchema).mutation(({ input }) => addEmployee(input)),
//     updateEmployee: publicProcedure.input(updateEmployeeSchema).mutation(({ input }) => updateEmployee(input)),
//     deleteEmployee: publicProcedure.input(deleteEmployeeSchema).mutation(({ input }) => deleteEmployee(input)),
// });

export const employeesRouter = router({
    getAllEmployees: securedProcedure.query(() => getEmployees()),
    getEmployee: securedProcedure.input(EmployeeIdSchema).query(({ input }) => getEmployee(input)),
    addEmployee: securedProcedure.input(insertEmployeeSchema).mutation(({ input }) => addEmployee(input)),
    updateEmployee: securedProcedure.input(updateEmployeeSchema).mutation(({ input }) => updateEmployee(input)),
    deleteEmployee: securedProcedure.input(deleteEmployeeSchema).mutation(({ input }) => deleteEmployee(input)),
});