import { LoginUserSchema } from "@/shared/schemas/user-schema";
import { TRPCError } from "@trpc/server";
import { FastifyReply } from "fastify";
import { stat } from "fs";
import jwt from 'jsonwebtoken';

export const loginHandler = async (input: LoginUserSchema, res: FastifyReply) => {
    console.log("loginHandler input: ", input);
    try {
        // Code to get the user from the Database
        const user = {
            id: '1',
            name: 'Manish',
            email: 'manish@abc.com',
            password: 'Manish@123'
        };

        if (input.email !== user.email) {
            throw new TRPCError({
                code: "UNAUTHORIZED",
                message: 'Invalid email',
            });
        } else if (input.password !== user.password) {
            throw new TRPCError({
                code: "UNAUTHORIZED",
                message: 'Invalid password',
            });
        } else {
            const secret = process.env.JWT_SECRET_KEY!;
            const token = jwt.sign({ sub: user.id }, secret, { expiresIn: 60 * 60 });

            res.setCookie('app-access-token', token, {
                httpOnly: true,
                path: '/',
                secure: process.env.NODE_ENV !== 'development',
                maxAge: 60 * 60, // Not setting maxAge makes it a session cookie
            });

            return { status: 'success', token };
        }

    } catch (error) {
        console.log(error);
        throw new TRPCError({
            code: "INTERNAL_SERVER_ERROR",
            message: 'Failed to login user',
            cause: error,
        });
    }
};