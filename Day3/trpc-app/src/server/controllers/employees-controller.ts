import { TRPCError } from "@trpc/server";
import { employeesDAO } from "../data-access";
import { DeleteEmployeeSchema, InsertEmployeeSchema, UpdateEmployeeSchema } from "@/shared/schemas/employee-schema";
import { z } from "zod";

export const getEmployees = async () => {
    try {
        const employees = await employeesDAO.getEmployees();
        return employees;
    } catch (error) {
        throw new TRPCError({
            code: "INTERNAL_SERVER_ERROR",
            message: 'Failed to retrieve employees',
            cause: error,
        });
    }
}

export const getEmployee = async (input: number) => {
    try {
        const employee = await employeesDAO.getEmployee(input);
        return employee;
    } catch (error) {
        throw new TRPCError({
            code: "INTERNAL_SERVER_ERROR",
            message: 'Failed to retrieve employee',
            cause: error,
        });
    }
}

export const addEmployee = async (input: InsertEmployeeSchema) => {
    try {
        const insertedEmployee = await employeesDAO.insertEmployee(input);
        return insertedEmployee;
    } catch (error: any) {
        if (error instanceof z.ZodError) {
            // Handle Zod validation errors
            throw new TRPCError({
                code: "BAD_REQUEST",
                message: "Validation error",
                cause: error.errors, // Include the validation error details
            });
        }

        throw new TRPCError({
            code: "INTERNAL_SERVER_ERROR",
            message: 'Failed to add employee',
            cause: error,
        });
    }
}

export const updateEmployee = async (input: UpdateEmployeeSchema) => {
    try {
        const updatedEmployee = await employeesDAO.findAndUpdateEmployee(input.id, input);
        return updatedEmployee;
    } catch (error) {
        if (error instanceof z.ZodError) {
            // Handle Zod validation errors
            throw new TRPCError({
                code: "BAD_REQUEST",
                message: "Validation error",
                cause: error.errors, // Include the validation error details
            });
        }

        throw new TRPCError({
            code: "INTERNAL_SERVER_ERROR",
            message: 'Failed to update employee',
            cause: error,
        });
    }
}

export const deleteEmployee = async (input: DeleteEmployeeSchema) => {
    try {
        await employeesDAO.findAndDeleteEmployee(input.id);
    } catch (error) {
        if (error instanceof z.ZodError) {
            // Handle Zod validation errors
            throw new TRPCError({
                code: "BAD_REQUEST",
                message: "Validation error",
                cause: error.errors, // Include the validation error details
            });
        }

        throw new TRPCError({
            code: "INTERNAL_SERVER_ERROR",
            message: 'Failed to delete employee',
            cause: error,
        });
    }
}