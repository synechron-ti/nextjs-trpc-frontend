import { initTRPC, TRPCError } from "@trpc/server";
import { Context } from "../context";
import superjson from "superjson";

export const t = initTRPC.context<Context>().create({
    transformer: superjson
});

const isAuthenticated = t.middleware(({ next, ctx }) => {
    if (!ctx.user) {
        throw new TRPCError({ code: 'UNAUTHORIZED', message: 'You must be logged in to access this resource' });
    }
    return next();
});

export const router = t.router;
export const publicProcedure = t.procedure;
export const securedProcedure = t.procedure.use(isAuthenticated);